package chompybird.chompybird.impl;

import chompybird.chompybird.lib.Status;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class FillBelows extends Task {
    private final String SUCK_ACTION = "Suck";
    private final String SWAMP_BUBBLES_NAME = "Swamp bubbles";
    private final String OGRE_BELLOWS_NAME = "Ogre bellows";

    @Override
    public boolean validate() {
        return needToFillBelow();
    }

    @Override
    public int execute() {
        Status.getInstance().setCurrentTask("Filling bellows");
        fill();
        return 50;
    }

    private boolean needToFillBelow() {
        return Inventory.getCount(OGRE_BELLOWS_NAME) > 0;
    }

    private boolean fill() {
        SceneObject obj = SceneObjects.getNearest(SWAMP_BUBBLES_NAME);
        if (obj != null) {
            if (obj.interact(SUCK_ACTION)) {
                Time.sleepUntil(() -> !needToFillBelow(), 10000);
                return true;
            }
        }
        return false;
    }
}
