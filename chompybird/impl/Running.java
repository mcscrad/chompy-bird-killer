package chompybird.chompybird.impl;

import chompybird.chompybird.lib.Status;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.script.task.Task;

public class Running extends Task {
    private int nextRunToggle = setNextRunTogglePercent();

    @Override
    public boolean validate() {
        return !Movement.isRunEnabled() && Movement.getRunEnergy() >= nextRunToggle;
    }

    @Override
    public int execute() {
        Status.getInstance().setCurrentTask("Toggling run");
        if (Movement.toggleRun(true)) {
            Time.sleepUntil(Movement::isRunEnabled, Random.nextInt(1000, 2000));
            nextRunToggle = setNextRunTogglePercent();
        }
        return 300;
    }

    private int setNextRunTogglePercent() {
        return Random.nextInt(20, 50);
    }
}
