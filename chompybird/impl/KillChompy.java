package chompybird.chompybird.impl;

import chompybird.chompybird.lib.Constants;
import chompybird.chompybird.lib.Status;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;

public class KillChompy extends Task {
    private final String ATTACK_ACTION = "Attack";
    private final Predicate<Npc> NEAREST_ATTACKABLE_CHOMPY = new Predicate<Npc>() {
        @Override
        public boolean test(Npc npc) {
            return npc.getName().equals(Constants.CHOMPY_BIRD_NAME) &&
                    npc.getHealthPercent() > 0 &&
                    npc.containsAction(ATTACK_ACTION);
        }
    };

    @Override
    public boolean validate() {
        return isChompy();
    }

    @Override
    public int execute() {
        Status.getInstance().setCurrentTask("Killing");
        if (!inCombat())
            kill();
        return 50;
    }

    private Npc nearestChompy() {
        return Npcs.getNearest(NEAREST_ATTACKABLE_CHOMPY);
    }

    private boolean isChompy() {
        return nearestChompy() != null;
    }

    private boolean inCombat() {
        return Players.getLocal().getTarget() != null;
    }

    private boolean kill() {
        Npc npc = nearestChompy();
        if (npc != null) {
            if (npc.interact(ATTACK_ACTION)) {
                Time.sleepUntil(this::inCombat, Random.nextInt(3000, 5000));
                return true;
            }
        }
        return false;
    }
}
