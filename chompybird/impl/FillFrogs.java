package chompybird.chompybird.impl;

import chompybird.chompybird.lib.Constants;
import chompybird.chompybird.lib.Status;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;

public class FillFrogs extends Task {
    private final String INFLATE_ACTION = "Inflate";
    private final String[] FILLED_OGRE_BELLOW_NAMES = new String[]{
            "Ogre bellows (1)", "Ogre bellows (2)", "Ogre bellows (3)"
    };
    private final Predicate<Npc> NEAREST_FILLABLE_FROG = new Predicate<Npc>() {
        @Override
        public boolean test(Npc npc) {
            return npc.getName().equals(Constants.TOAD_NAME) &&
                    npc.containsAction(INFLATE_ACTION);
        }
    };

    @Override
    public boolean validate() {
        return baitCount() < 3 && Inventory.getCount(FILLED_OGRE_BELLOW_NAMES) > 0;
    }

    @Override
    public int execute() {
        Status.getInstance().setCurrentTask("Filling frogs");
        fillFrog();
        return 10;
    }

    private Integer baitCount() {
        return Inventory.getCount(Constants.BAIT_NAME);
    }

    private boolean fillFrog() {
        Npc npc = Npcs.getNearest(NEAREST_FILLABLE_FROG);
        if (npc != null) {
            Integer count = baitCount();
            if (npc.interact(INFLATE_ACTION)) {
                Time.sleepUntil(() -> baitCount() > count, Random.nextInt(5000, 7000));
                return true;
            }
        }
        return false;
    }
}
