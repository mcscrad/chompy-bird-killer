package chompybird.chompybird.impl;

import chompybird.chompybird.lib.Constants;
import chompybird.chompybird.lib.Status;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.List;
import java.util.function.Predicate;

public class PlaceTrap extends Task {
    private final String DROP_ACTION = "Drop";
    private final Predicate<Position> NEAREST_EMPTY_POSITION = new Predicate<Position>() {
        @Override
        public boolean test(Position p) {
            return p.isPositionWalkable() &&
                    !hasBait(p);
        }
    };

    @Override
    public boolean validate() {
        return Inventory.getFirst(Constants.BAIT_NAME) != null;
    }

    @Override
    public int execute() {
        if (canPlaceTrap()) {
            Status.getInstance().setCurrentTask("Placing trap");
            placeTrap();
        } else {
            Status.getInstance().setCurrentTask("Walking to empty tile");
            walkToEmptyPosition();
        }
        return 50;
    }

    private boolean canPlaceTrap() {
        Position myPos = Players.getLocal().getPosition();
        return Constants.TRAP_AREA.contains(myPos) &&
                !hasBait(myPos);
    }

    private boolean placeTrap() {
        Item item = Inventory.getFirst(Constants.BAIT_NAME);
        if (item != null) {
            int count = Inventory.getCount(Constants.BAIT_NAME);
            if (item.interact(DROP_ACTION)) {
                Time.sleepUntil(() -> Inventory.getCount(Constants.BAIT_NAME) < count &&
                        !Players.getLocal().isMoving() &&
                        !Players.getLocal().isAnimating(), Random.nextInt(3000, 5000));
                return true;
            }
        }
        return false;
    }

    private boolean walkToEmptyPosition() {
        List<Position> list = Constants.TRAP_AREA.getTiles();
        Position p = Distance.getNearest(list.toArray(new Position[list.size()]), NEAREST_EMPTY_POSITION);
        if (p != null) {
            if (Movement.walkTo(p)) {
                Time.sleepUntil(() -> Players.getLocal().getPosition().equals(p), Random.nextInt(2000, 3000));
                return true;
            }
        }
        return false;
    }

    private boolean hasBait(Position pos) {
        for (Npc npc : Npcs.getLoaded(npc -> npc.getName().equals(Constants.BAIT_GROUND_NAME))) {
            if (npc.getPosition().equals(pos)) return true;
        }
        return false;
    }
}
