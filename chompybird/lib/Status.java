package chompybird.chompybird.lib;

public class Status {
    private int kills;
    private String currentTask;

    private Status() {
        this.kills = 0;
        this.currentTask = "Init";
    }
    private static Status instance;

    public static Status getInstance(){
        if (instance == null){
            instance = new Status();
        }
        return instance;
    }

    public int getKills() {
        return kills;
    }

    public void incrementKills() {
        kills++;
    }

    public String getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(String currentTask) {
        this.currentTask = currentTask;
    }
}
