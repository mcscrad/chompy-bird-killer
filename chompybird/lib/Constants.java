package chompybird.chompybird.lib;

import org.rspeer.runetek.api.movement.position.Area;

public class Constants {
    public static final String CHOMPY_BIRD_NAME = "Chompy bird";
    public static final String TOAD_NAME = "Swamp toad";
    public static final String BAIT_NAME = "Bloated toad";
    public static final String BAIT_GROUND_NAME = "Bloated Toad";

    public static final Area TRAP_AREA = Area.rectangular(
            2328, 3056, 2336, 3060, 0
    );

}
