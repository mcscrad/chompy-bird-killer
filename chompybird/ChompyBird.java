package chompybird.chompybird;

import chompybird.chompybird.impl.*;
import chompybird.chompybird.lib.Constants;
import chompybird.chompybird.lib.Status;
import org.rspeer.runetek.adapter.scene.PathingEntity;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.DeathListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.DeathEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

import java.awt.*;

@ScriptMeta(version = 0.11, developer = "Mcscrad", desc = "Kills chompy birds", name = "Chompy bird killer")
public class ChompyBird extends TaskScript implements RenderListener, DeathListener {
    private StopWatch startTime = StopWatch.start();
    private final Task[] TASKS = {
            new Running(),
            new KillChompy(),
            new PlaceTrap(),
            new FillFrogs(),
            new FillBelows()
    };

    @Override
    public void onStart() {
        submit(TASKS);
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        Graphics g = renderEvent.getSource();
        Status status = Status.getInstance();

        int killsPerHour = (int) startTime.getHourlyRate(status.getKills());

        g.drawString("Version: " + getMeta().version(), 10, 35);
        g.drawString("Chompies killed (/hr): " + status.getKills() + " (" + killsPerHour + ")", 10, 50);
        g.drawString("Has target: " + (Players.getLocal().getTarget() != null), 10, 70);
        g.drawString("Current Task: " + status.getCurrentTask(), 10, 90);
        g.drawString("Elapsed time: " + startTime.toElapsedString(), 10, 110);
    }

    @Override
    public void notify(DeathEvent deathEvent) {
        PathingEntity entity = deathEvent.getSource();
        if (entity.getName().equals(Constants.CHOMPY_BIRD_NAME)) {
            Status.getInstance().incrementKills();
        }
    }
}
